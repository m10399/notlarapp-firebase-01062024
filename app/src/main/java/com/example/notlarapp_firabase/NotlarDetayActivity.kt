package com.example.notlarapp_firabase

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.notlarapp_firabase.databinding.ActivityNotlarDetayBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class NotlarDetayActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNotlarDetayBinding
    private lateinit var not:Notlar
    private lateinit var refNotlar:DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityNotlarDetayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val db = FirebaseDatabase.getInstance()
        refNotlar = db.getReference("notlar")

        binding.toolbarDetayActivity.title = "Not Detay"
        setSupportActionBar(binding.toolbarDetayActivity)

        not = intent.getSerializableExtra("nesne") as Notlar
        binding.editTextDersAdiDetay.setText(not.ders_adi)
        binding.editTextNot1Detay.setText(not.not1.toString())
        binding.editTextNot2Detay.setText(not.not2.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.action_sil->{
                Snackbar.make(binding.toolbarDetayActivity,"Silinsin mi?",Snackbar.LENGTH_LONG).setAction("EVET"){
                    if (refNotlar != null && not.not_id != null){
                        refNotlar.child(not.not_id).removeValue()
                    }

                    startActivity(Intent(this@NotlarDetayActivity,MainActivity::class.java))
                    finish()
                }.show()
                return true
            }
            R.id.action_duzenle->{

                val ders_adi = binding.editTextDersAdiDetay.text.toString().trim()
                val not1 = binding.editTextNot1Detay.text.toString().trim()
                val not2 = binding.editTextNot2Detay.text.toString().trim()

                if (TextUtils.isEmpty(ders_adi)){
                    Snackbar.make(binding.root,"Ders adini giriniz",Snackbar.LENGTH_SHORT).show()
                }
                if (TextUtils.isEmpty(not1)){
                    Snackbar.make(binding.root,"1. notunu giriniz",Snackbar.LENGTH_SHORT).show()
                }
                if (TextUtils.isEmpty(not2)){
                    Snackbar.make(binding.root,"2. notunu giriniz",Snackbar.LENGTH_SHORT).show()
                }
                val bilgiler = HashMap<String,Any>()
                bilgiler.put("ders adi",ders_adi)
                bilgiler.put("not1",not1)
                bilgiler.put("not2",not2)
                refNotlar.child(not.not_id).updateChildren(bilgiler)
                startActivity(Intent(this@NotlarDetayActivity,MainActivity::class.java))
                finish()
                return true
            }
            else-> return false
        }
        return super.onOptionsItemSelected(item)
    }
}