package com.example.notlarapp_firabase

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.notlarapp_firabase.databinding.ActivityNotlarKayitBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class NotlarKayitActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNotlarKayitBinding
    private lateinit var refNotlar:DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityNotlarKayitBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val db = FirebaseDatabase.getInstance()
        refNotlar = db.getReference("notlar")
        binding.buttonKayit.setOnClickListener{
            val ders_adi = binding.editTextDersAdi.text.toString().trim()
            val not1 = binding.editTextNot1.text.toString().trim()
            val not2 = binding.editTextNot2.text.toString().trim()

            if (TextUtils.isEmpty(ders_adi)){
                Snackbar.make(binding.root,"Ders adini giriniz",Snackbar.LENGTH_SHORT).show()
            }
            if (TextUtils.isEmpty(not1)){
                Snackbar.make(binding.root,"1. notunu giriniz",Snackbar.LENGTH_SHORT).show()
            }
            if (TextUtils.isEmpty(not2)){
                Snackbar.make(binding.root,"2. notunu giriniz",Snackbar.LENGTH_SHORT).show()
            }
            val not = Notlar("",ders_adi,not1.toInt(),not2.toInt())

            refNotlar.push().setValue(not)
            startActivity(Intent(this@NotlarKayitActivity,MainActivity::class.java))
        }
    }
}