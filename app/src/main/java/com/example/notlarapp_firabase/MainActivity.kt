package com.example.notlarapp_firabase

import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.notlarapp_firabase.databinding.ActivityMainBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MainActivity : AppCompatActivity() {
    private lateinit var  binding: ActivityMainBinding
    private lateinit var notlarListe: ArrayList<Notlar>
    private lateinit var adapter: NotlarAdapter
    private lateinit var refNotlar:DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        binding.toolbarMainActivity.title = "Not Kayıt"

        setSupportActionBar(binding.toolbarMainActivity)

        binding.rv.setHasFixedSize(true)
        binding.rv.layoutManager = LinearLayoutManager(this)

        notlarListe = ArrayList()
        val db = FirebaseDatabase.getInstance()
        refNotlar = db.getReference("notlar")

        tumNotlar()
        adapter = NotlarAdapter(this,notlarListe)

        binding.rv.adapter = adapter

        binding.fab.setOnClickListener{
            startActivity(Intent(this@MainActivity,NotlarKayitActivity::class.java))
        }
    }

    fun tumNotlar(){
        refNotlar.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var toplam = 0
                notlarListe.clear()
                for (c in snapshot.children){
                    val not = c.getValue(Notlar::class.java)
                    if (not != null){
                        not.not_id= c.key!!
                        notlarListe.add(not)
                        toplam = toplam+ (not.not1!!+not.not2!!)/2
                    }
                }
                adapter.notifyDataSetChanged()

                if (toplam != 0){
                    binding.toolbarMainActivity.subtitle = "Not Ortalamasi : ${toplam/notlarListe.size}"
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
    }
}